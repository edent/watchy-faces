# Watchy Faces

Here's my daft watch faces for [the eInk Watchy](https://shkspr.mobi/blog/2023/06/review-watchy-an-eink-watch-full-of-interesting-compromises/).

## Features!
Handily displays the time in the following formats ALL AT ONCE

* ISO8601 / RFC3339
* Day of the week
* Unix Epoch
* Swatch Internet Time / Decimal Time
* Date on Mars
* Fuzzy / Whimsical Time

Also includes a TOTP MFA passcode generator and battery level.

## Thanks To

* [Watchy Examples](https://github.com/sqfmi/Watchy) (MIT)
* [TOTP-Arduino](https://github.com/lucadentella/TOTP-Arduino/) ([MIT](https://github.com/lucadentella/TOTP-Arduino/issues/1))
* [GNU Unifont](https://unifoundry.com/unifont/index.html) (SIL OFL 1.1)
