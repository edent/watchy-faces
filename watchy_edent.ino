#include <Watchy.h> //include the Watchy library
#include "latin8pt7b.h" //include any fonts you want to use
#include "settings.h" //same file as the one from 7_SEG example

//  For TOTP https://github.com/lucadentella/TOTP-Arduino/
#include "sha1.h"
#include "TOTP.h"

class MyFirstWatchFace : public Watchy{ //inherit and extend Watchy class
    public:
        MyFirstWatchFace(const watchySettings& s) : Watchy(s) {}
        void drawWatchFace(){ //override this method to customize how the watch face looks
          display.fillScreen(GxEPD_WHITE);  //  White background
          display.setTextColor(GxEPD_BLACK);  //  Black text
          display.setFont(&latin8pt7b);
          display.setCursor(0, 10); //  Start in top right, dropped by the font size + a little padding

          //  ISO Date
          display.println("ISO8601 / RFC3339 time:");

          display.print(tmYearToCalendar(currentTime.Year));// offset from 1970, since year is stored in uint8_t
          display.print("-");
          if(currentTime.Month < 10){ //  Leading zero
            display.print("0");
          }
          display.print(currentTime.Month);
          display.print("-");
           if(currentTime.Day < 10){ //  Leading zero
            display.print("0");
          }
          display.print(currentTime.Day);
          display.print("T");

          if(currentTime.Hour < 10){ //  Leading zero
            display.print("0");
          }
          display.print(currentTime.Hour);
          display.print(":");
          if(currentTime.Minute < 10){
            display.print("0");
          }  
          display.print(currentTime.Minute); 
          display.print(":");
          if(currentTime.Second < 10){ //  Leading zero
            display.print("0");
          }
          display.print(currentTime.Second);
          display.print("+01:00");

          //  Day of the week
          display.println();
          switch (currentTime.Wday) {
            case 0:
              display.print("Saturday"); //  Dunno if it is Zero based
              break;
            case 1:
              display.print("Sunday");
              break;
            case 2:
              display.print("Monday");
              break;
            case 3:
              display.print("Tuesday");
              break;
            case 4:
              display.print("Wednesday");
              break;
            case 5:
              display.print("Thursday");
              break;
            case 6:
              display.print("Friday");
              break;
            case 7:
              display.print("Saturday");
              break;
          }
          
          //  Unix Epoch
          display.println("");
          display.print("Unix Epoch: ");
          RTC.read(currentTime);
          time_t epoch = makeTime(currentTime) - 3600; // BST offset
          display.print( epoch );

          //  Swatch Internet Time
          float secondsInABeat = 86.4;
          int8_t h = currentTime.Hour;
          int8_t m = currentTime.Minute;
          int8_t s = currentTime.Second;

          // Only needed if in GMT. Beats are in UTC+1
          // if ( h == 23 ) { h = 0; } else { h = h+1; }
          int swissTimeInSeconds = (((h * 60) + m) * 60) + s;
          
          float beats = swissTimeInSeconds / secondsInABeat;
          display.println("");
          display.print("Swatch Time: @");
          display.printf("%.2f", beats);

          //  Mars Time https://marsclock.com/
          float leapSeconds = 37 + 32.184;
          float terrestrialTimeDays = ( epoch + leapSeconds ) / 86400;
          float j2000Days = terrestrialTimeDays - 2451545 + 2440587.5;
          float marsSolDate = ( ( j2000Days - 4.5 ) / 1.027491252 ) + 44796.0 - 0.00096;
          display.println("");
          display.print("Mars Sol Date: ");
          display.printf("%.2f", marsSolDate);

          //  Fuzzy Time
          display.println("");

          int8_t hour = currentTime.Hour;
          if (hour >= 5 && hour < 7) {
              display.println("Blimey! That's early.");
          } else if (hour >= 7 && hour < 11) {
              display.println("Good morning!\nRise and shine!");
          } else if (hour >= 11 && hour < 13) {
              display.println("Goodness me! Elevenses!");
          } else if (hour >= 13 && hour < 17) {
              display.println("Afternoon tea time!\nCare for a cuppa?");
          } else if (hour >= 17 && hour < 20) {
              display.println("Evening is upon us.\nTime to unwind.");
          } else if (hour >= 20 && hour < 23) {
              display.println("Nighttime adventures await!\nOff we go!");
          } else {
              display.println("Bedtime beckons.\nRest well, my friend.");
          }

          //  TOTP
          display.print( "TOTP Code: ");
          
          // The shared secret is MyLegoDoor - convert at https://www.lucadentella.it/OTP/
          uint8_t hmacKey[] = {0x4d, 0x79, 0x4c, 0x65, 0x67, 0x6f, 0x44, 0x6f, 0x6f, 0x72};
          int hmacKeyLength = sizeof(hmacKey) / sizeof(hmacKey[0]);

          TOTP totp = TOTP(hmacKey, hmacKeyLength);
          char* epochCode = totp.getCode( epoch );
          display.println( epochCode );          

          //  Battery Level
          display.println();
          float blow = 3.45; // https://github.com/GuruSR/SmallRTC/blob/main/SmallRTC.cpp#L223
          float size = ((getBatteryVoltage() - blow) / (4.2 - blow));
          int8_t batteryLevel = size * 100;
          display.print("Battery: ");
          display.print(batteryLevel);
          display.print("%");
        }
};

MyFirstWatchFace m(settings); //instantiate your watchface

void setup() {
  m.init(); //call init in setup
}

void loop() {
  // this should never run, Watchy deep sleeps after init();
}